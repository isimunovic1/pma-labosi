package com.example.lab9

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class CustomAdapter(applicationContext: Context, jobs: List<Work>) :
    BaseAdapter() {
    var context: Context
    var jobs: List<Work>
    var inflter: LayoutInflater
    override fun getCount(): Int {
        return jobs.size
    }

    override fun getItem(i: Int): Any? {
        return null
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        var view = view
        view = inflter.inflate(R.layout.list_items, null)
        val title = view.findViewById<TextView>(R.id.firstLine)
        val detail = view.findViewById<TextView>(R.id.secondLine)
        val indicator = view.findViewById<ImageView>(R.id.indicator)
        title.text = jobs[i].title
        detail.text = jobs[i].detail
        if (jobs[i].finished) {
            indicator.visibility = View.VISIBLE
        } else {
            indicator.visibility = View.INVISIBLE
        }
        return view
    }

    init {
        context = applicationContext
        this.jobs = jobs
        inflter = LayoutInflater.from(applicationContext)
    }
}