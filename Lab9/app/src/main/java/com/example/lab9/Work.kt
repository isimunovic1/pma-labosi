package com.example.lab9

data class Work(var title: String= "", var detail: String = "", var finished: Boolean = false)