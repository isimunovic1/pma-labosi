package com.example.lab9

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var simpleList: ListView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        simpleList = findViewById(R.id.listView)
        val works = mutableListOf<Work>();
        val work1 = Work("Work1", "Work placeholder", true);
        val work2 = Work("Work2", "Work placeholder", false);
        val work3 = Work("Work3", "Work placeholder", true);
        val work4 = Work("Work4", "Work placeholder", false);
        val work5 = Work("Work5", "Work placeholder", false);
        val work6 = Work("Work6","Work placeholer",true);
        works.add(work1); works.add(work2); works.add(work3); works.add(work4); works.add(work5); works.add(work6);
        var customAdapter = CustomAdapter(this, works)

        simpleList.adapter = customAdapter
    }

}