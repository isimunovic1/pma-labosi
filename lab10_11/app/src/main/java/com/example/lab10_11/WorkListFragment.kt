package com.example.lab10_11

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.inflate
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


private const val TAG = "WorkListFragment"
class WorkListFragment : Fragment() {

    private lateinit var workRecyclerView: RecyclerView
    private var adapter: WorkAdapter? = WorkAdapter(emptyList())



    private val workListViewModel : WorkListViewModel by lazy {
        ViewModelProviders.of(this).get(WorkListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_work_list, container, false)
        workRecyclerView = view.findViewById(R.id.work_recycler_view) as RecyclerView
        workRecyclerView.layoutManager = LinearLayoutManager(context)
        workRecyclerView.adapter = adapter


        return view;
    }
    private fun updateUI(jobs: List<Work>) {
        adapter = WorkAdapter(jobs)
        workRecyclerView.adapter = adapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        workListViewModel.jobsLiveData.observe(
                viewLifecycleOwner,
                Observer { jobs ->
                    jobs?.let {
                        Log.i(TAG, "Got jobs! ${jobs.size}")
                        updateUI(jobs)
                    }
                } )
    }

    private inner class WorkHolder (view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private lateinit var work: Work
        private  val titleTextView: TextView = itemView.findViewById(R.id.work_title)
        private val dateTextView: TextView = itemView.findViewById(R.id.work_date)
        private val solvedImageView: ImageView = itemView.findViewById(R.id.solvedIndicator)

        init {
            itemView.setOnClickListener(this)
        }
        fun bind(work: Work) {
            this.work = work
            titleTextView.text = work.title
            dateTextView.text = work.date.toString()
            solvedImageView.visibility = if(work.isDone) {
                View.VISIBLE;
            } else {
                View.GONE;
            }
        }

        override fun onClick(v: View?) {
            Toast.makeText(context, "${work.title} pressed!", Toast.LENGTH_SHORT).show()
        }

    }

    private inner class WorkAdapter(val jobs: List<Work>) :RecyclerView.Adapter<WorkHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkHolder {
           val view = layoutInflater.inflate(R.layout.list_item_work, parent,false )
            return WorkHolder(view);
        }



        override fun getItemCount(): Int {
            return jobs.size
        }

        override fun onBindViewHolder(holder: WorkHolder, position: Int) {
            val work = jobs [position]
            holder.bind(work)
        }

    }


    companion object {
        fun newInstance(): WorkListFragment {
            return WorkListFragment()
        }
    }

}