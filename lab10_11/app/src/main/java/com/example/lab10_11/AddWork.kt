package com.example.lab10_11

import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class AddWork : AppCompatActivity(){
    private lateinit var work: Work
    private lateinit var titleField: EditText
    private lateinit var dateButton: Button
    private lateinit var checkDone: CheckBox
    private lateinit var saveButton: Button
    private lateinit var cancelButton: Button
    private  val workRepository = WorkRepository.get()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_work)
        work = Work()
        titleField = findViewById(R.id.work_title)
        dateButton = findViewById(R.id.work_date)
        checkDone = findViewById(R.id.work_done)
        saveButton = findViewById(R.id.saveButton)
        cancelButton = findViewById(R.id.cancelButton)

        dateButton.apply {
            text = work.date.toString()
            isEnabled = false
        }

         checkDone.setOnCheckedChangeListener { buttonView, isChecked ->
            work.isDone = isChecked
        }

        saveButton.setOnClickListener{
            createJob()
        }

        cancelButton.setOnClickListener{
            finish()
        }
    }

    private fun createJob(){
        work.title = titleField.text.toString()
        workRepository.addJob(work)
    }
}