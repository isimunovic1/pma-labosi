package com.example.lab10_11

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import java.lang.IllegalStateException
import java.util.*
import java.util.concurrent.Executors

private const val DATABASE_NAME = "work-database"

class WorkRepository private constructor(context: Context) {
    private val database: Database =
            Room.databaseBuilder(
                    context.applicationContext,
                    Database::class.java,
                    DATABASE_NAME
            ).build()
    private val workDao = database.workDao()
    private val executor = Executors.newSingleThreadExecutor()

    fun getJobs(): LiveData<List<Work>> = workDao.getJobs()
    fun getJob(id: UUID): LiveData<Work?> = workDao.getJob(id)

    companion object {
        private var INSTANCE: WorkRepository? = null
        fun initialize(context: Context) {
            if(INSTANCE == null) {
                INSTANCE = WorkRepository(context)
            }
        }
        fun get(): WorkRepository {
            return INSTANCE ?: throw IllegalStateException("WorkRepository must be initialized!")
        }
    }

    fun updateJob(work: Work){
        executor.execute{
            workDao.updateJob(work)
        }
    }
    fun addJob(work: Work){
        executor.execute{
            workDao.addJob(work)
        }
    }

}