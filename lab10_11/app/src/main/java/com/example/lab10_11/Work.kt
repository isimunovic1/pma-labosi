package com.example.lab10_11

import java.util.*
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Work (@PrimaryKey val id: UUID = UUID.randomUUID(),
                 var title: String = "",
                 var date: Date = Date(),
                 var isDone: Boolean = false)
