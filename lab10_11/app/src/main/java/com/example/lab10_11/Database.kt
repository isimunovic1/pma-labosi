package com.example.lab10_11
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters

@Database(entities = [Work::class], version = 1)
@TypeConverters(WorkTypeConverters::class)
abstract class Database:RoomDatabase() {
    abstract  fun workDao():WorkDao
}