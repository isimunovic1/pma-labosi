package com.example.lab10_11
import androidx.lifecycle.ViewModel

class WorkListViewModel : ViewModel() {
/*
    val jobs = mutableListOf<Work>()

    init {
        for (i in 0 until 10) {
            val work = Work()
            work.title = "Work placeholder!"
            work.isDone =  i%2 == 0
            jobs.add(work)
        }
    }*/
    private val workRepository = WorkRepository.get()
    val jobsLiveData = workRepository.getJobs()

}