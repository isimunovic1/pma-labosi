package com.example.lab10_11

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class SelectActivity : AppCompatActivity(){

    private lateinit var showButton : Button
    private lateinit var addButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.select_activity)

        showButton = findViewById(R.id.showButton)
        addButton = findViewById(R.id.addButton)

        showButton.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        addButton.setOnClickListener{
            val intent = Intent(this, AddWork::class.java)
            startActivity(intent)
        }
    }
}