package com.example.lab10_11

import androidx.room.TypeConverter
import java.util.*

class WorkTypeConverters {
    @TypeConverter
    fun fromDate(date: Date?):Long?{
        return date?.time
    }
    @TypeConverter
    fun toDate(millisSinceWork: Long?):Date?{
        return millisSinceWork?.let {
            Date(it);
        }
    }

    @TypeConverter
    fun toUUID(uuid: String?): UUID?{
        return UUID.fromString(uuid)
    }

    @TypeConverter
    fun fromUUID(uuid: UUID?): String?{
        return uuid?.toString()
    }

}