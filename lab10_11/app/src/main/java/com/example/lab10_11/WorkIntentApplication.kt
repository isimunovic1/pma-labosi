package com.example.lab10_11

import android.app.Application
class WorkIntentApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        WorkRepository.initialize(this)
    }
}
