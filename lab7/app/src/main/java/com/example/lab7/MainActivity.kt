package com.example.lab7


import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private lateinit var ispisBtn : Button
    private lateinit var radioButton: RadioButton
    private lateinit var radioGroup: RadioGroup
    private lateinit var checkBox: CheckBox
    private lateinit var toggleButton: ToggleButton
    private lateinit var editText: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ispisBtn = findViewById(R.id.ispisBtn)
        radioGroup = findViewById(R.id.radioGroup)
        checkBox = findViewById(R.id.checkBox)
        toggleButton = findViewById(R.id.toggleButton)
        editText = findViewById(R.id.editText)
        ispisBtn.setOnClickListener{ v: View? ->
            ispisPodataka()
        }
        checkBox.setOnClickListener{ v: View? ->
            var ispis: String = ""
            if(checkBox.isChecked){
                ispis += "Checkbox oznacen"
            }
            else{
                ispis += "Checkbox nije oznacen"
            }
            Toast.makeText(this, ispis, Toast.LENGTH_LONG).show();
        }
        toggleButton.setOnClickListener{ v: View? ->
            var ispis: String = ""
            if(toggleButton.isChecked){
                ispis += "ToggleButton oznacen"
            }
            else{
                ispis += "ToggleButton nije oznacen"
            }
            Toast.makeText(this, ispis, Toast.LENGTH_LONG).show();
        }
        radioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val radio:RadioButton = group.findViewById(checkedId)
            var ispis: String = radio.text.toString()
            Toast.makeText(this, ispis, Toast.LENGTH_LONG).show();
        })
    }

    private fun ispisPodataka(){
        var ispis: String = ""
        ispis += editText.text
        ispis += ", "
        val id = radioGroup.checkedRadioButtonId
        if (id == -1) {
            ispis += " ,nije oznacen niti jedan radio button, "
        } else {
            val radio = findViewById<RadioButton>(id)
            ispis += radio.text
            ispis += ", "
        }
        if(checkBox.isChecked){
            ispis += "checkBox je oznacen, "
        }
        else{
            ispis += "checkBox nije oznacen, "
        }
        if(toggleButton.isChecked){
            ispis += "toggleBtn je On"
        }
        else{
            ispis += "toggleBtn je Off"
        }
        Toast.makeText(this, ispis, Toast.LENGTH_LONG).show();
    }
}